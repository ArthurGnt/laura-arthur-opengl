# Install script for directory: /Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/Library/Developer/CommandLineTools/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/Users/arthurgenet/Desktop/laura-arthur-opengl/build/external/assimp-5.0.0/code/libassimp.a")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimp.a" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimp.a")
    execute_process(COMMAND "/Library/Developer/CommandLineTools/usr/bin/ranlib" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimp.a")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xassimp-devx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/assimp" TYPE FILE FILES
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/anim.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/aabb.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/ai_assert.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/camera.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/color4.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/color4.inl"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/build/external/assimp-5.0.0/code/../include/assimp/config.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/defs.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/Defines.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/cfileio.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/light.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/material.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/material.inl"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/matrix3x3.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/matrix3x3.inl"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/matrix4x4.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/matrix4x4.inl"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/mesh.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/pbrmaterial.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/postprocess.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/quaternion.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/quaternion.inl"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/scene.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/metadata.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/texture.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/types.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/vector2.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/vector2.inl"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/vector3.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/vector3.inl"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/version.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/cimport.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/importerdesc.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/Importer.hpp"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/DefaultLogger.hpp"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/ProgressHandler.hpp"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/IOStream.hpp"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/IOSystem.hpp"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/Logger.hpp"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/LogStream.hpp"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/NullLogger.hpp"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/cexport.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/Exporter.hpp"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/DefaultIOStream.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/DefaultIOSystem.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/ZipArchiveIOSystem.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/SceneCombiner.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/fast_atof.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/qnan.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/BaseImporter.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/Hash.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/MemoryIOWrapper.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/ParsingUtils.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/StreamReader.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/StreamWriter.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/StringComparison.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/StringUtils.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/SGSpatialSort.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/GenericProperty.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/SpatialSort.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/SkeletonMeshBuilder.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/SmoothingGroups.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/SmoothingGroups.inl"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/StandardShapes.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/RemoveComments.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/Subdivision.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/Vertex.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/LineSplitter.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/TinyFormatter.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/Profiler.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/LogAux.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/Bitmap.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/XMLTools.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/IOStreamBuffer.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/CreateAnimMesh.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/irrXMLWrapper.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/BlobIOSystem.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/MathFunctions.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/Macros.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/Exceptional.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/ByteSwapper.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xassimp-devx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/assimp/Compiler" TYPE FILE FILES
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/Compiler/pushpack1.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/Compiler/poppack1.h"
    "/Users/arthurgenet/Desktop/laura-arthur-opengl/external/assimp-5.0.0/code/../include/assimp/Compiler/pstdint.h"
    )
endif()

