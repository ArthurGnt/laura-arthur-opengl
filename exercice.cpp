#include <iostream>
#include <cmath>

// GLEW
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>

// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Shader loading class
#include "Shader.h"
// Camera loading class
#include "Camera.h"
// Model loading class
#include "Model.h"

// Other Libs
#include <SOIL.h>

// Prototype of functions
void fillBuffer(GLuint &VAO, GLuint &VBO, GLuint &EBO, GLfloat vertices[], 
    GLshort indices[], GLint vsize, GLint isize);
void drawObject(GLuint &VAO, GLint nVertex, GLuint &texture, Shader &shaders);
void loadTexture(GLuint &texture, const char* path);

// Dimension of the window
const GLuint WIDTH = 800, HEIGHT = 600;

// Main function, it is here that we initialize what is relative to the context
// of OpenGL and that we launch the main loop
int main()
{
    std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
    // Initialization of GLFW
    glfwInit();

    // Specify the OpenGL version to be used (3.3)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //.3
    // Disable the deprecated functions
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    // Necesary for Mac OS 
    #ifdef __APPLE__
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif
    // Prevent the change of dimension of the window
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    
    // Create the application window
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "MyWindow", nullptr, nullptr);    
    if (window == nullptr)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    
    // Associate the created window with the OpenGL context
    glfwMakeContextCurrent(window);
    // Associate the callback with the pressure of a key on the keyboard
    glfwSetKeyCallback(window, key_callback);

    // Variable global that allows to ask the GLEW library to find the modern 
    // functions of OpenGL
    glewExperimental = GL_TRUE;
    // Initializing GLEW to retrieve pointers from OpenGL functions
    if (glewInit() != GLEW_OK)
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return -1;
    }    

    // Transform the window dimensions to the dimensions of OpenGL (between -1 and 1)
    int width, height;
    // Recover the dimensions of the window created above
    glfwGetFramebufferSize(window, &width, &height); 
    glViewport(0, 0, width, height);

    // Allow to test the depth information
    glEnable(GL_DEPTH_TEST);
    
    // Build and compile our vertex and fragment shaders
    Shader shaders("/home/tsi20/Documents/laura-arthur-opengl/shaders/vertexshader.glsl", 
        "/home/tsi20/Documents/laura-arthur-opengl/shaders/fragmentshader.glsl");

    // Load the model
    Model object((char *)"../model/planet/planet.obj");
    Model object2((char *)"../model/Streetlight/Streetlight_HighRes.obj");
    Model object3((char *)"../model/lowpolytree/lowpoyltree.obj");


    // House, roof and ground geometry separated (position + uv)
    GLfloat houseVertices[] = {  
         -4,  -4,   4,   0,   0,  0,0,1,           4,  -4,   4,   1,   0,  0,0,1,
          4,   4,   4,   1,   1,  0,0,1,          -4,   4,   4,   0,   1,  0,0,1,    // front face
         -4,  -4,  -4,   0,   0,  1,0,0,          -4,  -4,   4,   1,   0,  1,0,0,
         -4,   4,   4,   1,   1,  1,0,0,          -4,   4,  -4,   0,   1,  1,0,0,     // left face
          4,  -4,  -4,   0,   0,  0,0,-1,         -4,  -4,  -4,   1,   0,  0,0,-1,
         -4,   4,  -4,   1,   1,  0,0,-1,          4,   4,  -4,   0,   1,  0,0,-1,    // back face
          4,  -4,   4,   0,   0,  -1,0,0,          4,  -4,  -4,   1,   0, -1,0,0,
          4,   4,  -4,   1,   1,  -1,0,0,          4,   4,   4,   0,   1, -1,0,0,    // right face
          4,   4,   4,   0,   0,  0,1,0,           4,   4,  -4,   1,   0,  0,1,0,
         -4,   4,  -4,   1,   1,  0,1,0,          -4,   4,   4,   0,   1,  0,1,0,    // top face
         -4,  -4,   4,   0,   0,  0,-1,0,         -4,  -4,  -4,   1,   0,  0,-1,0,
          4,  -4,  -4,   1,   1,  0,-1,0,          4,  -4,   4,   0,   1,  0,-1,0,   // bottom face
    };   

    GLfloat houseVertices2[] = {  
         -15,  -4,   6,   0,   0,  0,0,1,              -10,  -4,   6,   1,   0, 0,0,1,
         -10,   4,   6,   1,   1,  0,0,1,              -15,   4,   6,   0,   1, 0,0,1,     // front face
         -15,  -4,  -6,   0,   0, -1,0,0,              -15,  -4,   6,   1,   0, -1,0,0,
         -15,   4,   6,   1,   1, -1,0,0,              -15,   4,  -6,   0,   1, -1,0,0,   // left face
         -10,  -4,  -6,   0,   0,  0,0,-1,             -15,  -4,  -6,   1,   0, 0,0,-1, 
         -15,   4,  -6,   1,   1,  0,0,-1,             -10,   4,  -6,   0,   1, 0,0,-1,    // back face
         -10,  -4,   6,   0,   0,  1,0,0,              -10,  -4,  -6,   1,   0, 1,0,0,
         -10,   4,  -6,   1,   1,  1,0,0,              -10,   4,   6,   0,   1, 1,0,0,   // right face
         -10,   4,   6,   0,   0,  0,1,0,              -10,   4,  -6,   1,   0, 0,1,0,
         -15,   4,  -6,   1,   1,  0,1,0,              -15,   4,   6,   0,   1, 0,1,0,  // top face
         -15,  -4,   6,   0,   0,  0,-1,0,             -15,  -4,  -6,   1,   0, 0,-1,0,
         -10,  -4,  -6,   1,   1,  0,-1,0,             -10,  -4,   6,   0,   1, 0,-1,0,    // bottom face
    };   

    GLshort houseIndices[] = {
            0,2,3,    0,1,2,      // front face
            4,6,7,    4,5,6,      // left face
            8,10,11,  8,9,10,     // back face
            12,14,15, 12,13,14,   // right face
            16,18,19, 16,17,18,   // top face
            20,22,23, 20,21,22,   // bottom face
    };

    GLfloat roofVertices[] = {  
         -4,   4,   4,   0,   0,  0, 1, 1,      4,   4,   4,   1,   0,  0, 1, 1,
          0,   8,   4, 0.5,   1,  0, 1, 1,                                        // front wall
          4,   4,   4,   0,   0,  1, 1, 0,      4,   4,  -4,   1,   0,  1, 1, 0,
          0,   8,  -4,   1,   1,  1, 1, 0,      0,   8,   4,   0,   1,  1, 1, 0,  // left slope
         -4,   4,   4,   0,   0, -1, 1, 0,      0,   8,   4,   0,   1, -1, 1, 0,
          0,   8,  -4,   1,   1, -1, 1, 0,     -4,   4,  -4,   1,   0, -1, 1, 0,  // right slope
          4,   4,  -4,   0,   0,  0, 1,-1,     -4,   4,  -4,   1,   0,  0, 1,-1,
          0,   8,  -4, 0.5,   1,  0, 1,-1,                                     // rear wall 
    };              


    GLfloat roofVertices2[] = {  
         -15,   4,   6,   0,   0,  0, 1, 1,        -10,   4,   6,   1,   0,  0, 1, 1,
         -12.5, 6,   6, 0.5,   1,  0, 1, 1,                                         // front wall
         -10,   4,   6,   0,   0,  1, 1, 0,        -10,   4,  -6,   1,   0,  1, 1, 0,
         -12.5, 6,  -6,   1,   1,  1, 1, 0,        -12.5, 6,   6,   0,   1,  1, 1, 0,    // left slope
         -15,   4,   6,   0,   0,  -1, 1, 0,       -12.5, 6,   6,   0,   1, -1, 1, 0, 
         -12.5, 6,  -6,   1,   1,  -1, 1, 0,       -15,   4,  -6,   1,   0, -1, 1, 0,   // right slope
         -10,   4,  -6,   0,   0,  0, 1,-1 ,       -15,   4,  -6,   1,   0,  0, 1,-1,
         -12.5, 6,  -6, 0.5,   1,  0, 1,-1 ,                                        // rear wall
    };            

    GLshort roofIndices[] = {
            0,1,2,                // front attic wall
            3,5,6, 3,4,5,         // left slope
            7,9,10, 7,8,9,        // right slope
            11,12,13,             // rear attic wall
    };

    GLfloat groundVertices[] = {  
        -20,  -4,  20,   0,   0,  0,1,0,             20,  -4,  20,   1,   0, 0,1,0,
         20,  -4, -20,   1,   1,  0,1,0,            -20,  -4, -20,   0,   1, 0,1,0,    // plane
    };              
           

    GLshort groundIndices[] = {
            0,2,3, 0,1,2,         // grass
    };
    
    // Declare the identifiers of our VAO, VBO and EBO
    GLuint VAO[5], VBO[5], EBO[5];
    // Inform OpenGL to generate one VAO
    glGenVertexArrays(5, VAO);
    // Inform OpenGL to generate one VBO
    glGenBuffers(5, VBO);
    // Inform OpenGL to generate one EBO
    glGenBuffers(5, EBO);

    // Fill the buffers with each object
    fillBuffer(VAO[0], VBO[0], EBO[0], houseVertices, houseIndices, sizeof(houseVertices), sizeof(houseIndices));
    fillBuffer(VAO[1], VBO[1], EBO[1], roofVertices, roofIndices, sizeof(roofVertices), sizeof(roofIndices));
    fillBuffer(VAO[2], VBO[2], EBO[2], groundVertices, groundIndices, sizeof(groundVertices), sizeof(groundIndices));
    fillBuffer(VAO[3], VBO[3], EBO[3], houseVertices2, houseIndices, sizeof(houseVertices2), sizeof(houseIndices));
    fillBuffer(VAO[4], VBO[4], EBO[4], roofVertices2, roofIndices, sizeof(roofVertices2), sizeof(roofIndices));

    // Declare the texture identifier
    GLuint texture[3]; 
    // Generate the texture
    glGenTextures(3, texture); 
    // Load the textures
    loadTexture(texture[0], "/home/tsi20/Documents/laura-arthur-opengl/texture/wood.jpg");  // house
    loadTexture(texture[1], "/home/tsi20/Documents/laura-arthur-opengl/texture/roof.jpg");  // roof
    loadTexture(texture[2], "/home/tsi20/Documents/laura-arthur-opengl/texture/grass.jpg");  // ground


    // Camera
    Camera camera(glm::vec3(0.0f, 0.0f, 7.0f), window);

    // The main loop of the program
    while (!glfwWindowShouldClose(window))
    {       
        // Retrieve events and call the corresponding callback functions
        glfwPollEvents();
        camera.Do_Movement();

        // Replace the background color buffer of the window
        glClearColor(0.f, 0.f, 0.f, 0.f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        // Inform OpenGL that we want to use the shaders we have created
        shaders.Use();

        // Recover the identifiers of the global variables of the shader
        GLint modelLoc = glGetUniformLocation(shaders.Program, "model");

        GLint modelLoc2 = glGetUniformLocation(shaders.Program, "model");
        GLint modelLoc3 = glGetUniformLocation(shaders.Program, "model");
        GLint viewLoc  = glGetUniformLocation(shaders.Program, "view");
        GLint projLoc  = glGetUniformLocation(shaders.Program, "projection");
        GLint lightPos = glGetUniformLocation(shaders.Program, "lightPos");
        GLint lightColor = glGetUniformLocation(shaders.Program,"lightColor");
        // Model matrix (translation, rotation and scale)
        glm::mat4 model = glm::mat4(1.0f);
        
        GLfloat timeValue = glfwGetTime();
        GLfloat radius = 10.0f;
        glm::vec3 lPos(glm::vec3(radius*cos(timeValue), radius*sin(timeValue), 10.f));
        glm::vec3 lColor(1.0f, 1.0f, 1.0f);
        glUniform3f(lightPos,lPos.x, lPos.y, lPos.z);
        glUniform3f(lightColor,lColor.x, lColor.y, lColor.z);

        model = glm::translate(model, glm::vec3(0.0f));
        model = glm::scale(model, glm::vec3(0.3f));

        glm::mat4 model2 = glm::mat4(1.0f);
        model2 = glm::translate(model2, glm::vec3(0.0f));
        model2 = glm::scale(model2, glm::vec3(0.3f));

        glm::mat4 model3 = glm::mat4(1.0f);
        model3 = glm::translate(model3, glm::vec3(0.0f));
        model3 = glm::scale(model3, glm::vec3(0.3f));


        // Camera matrix
        glm::mat4 view;
        view = camera.GetViewMatrix();
        // Projection matrix (generate a perspective projection matrix)
        glm::mat4 projection;
        projection = glm::perspective(45.0f, (GLfloat)WIDTH / (GLfloat)HEIGHT, 0.1f, 100.0f);

        // Update the global variables of the shader
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

        glUniformMatrix4fv(modelLoc2, 1, GL_FALSE, glm::value_ptr(model2));
        glUniformMatrix4fv(modelLoc3, 1, GL_FALSE, glm::value_ptr(model3));
        
        // Draw objects 
        
        drawObject(VAO[0], 12, texture[0], shaders);  // house
        drawObject(VAO[1], 6, texture[1], shaders);   // roof
        drawObject(VAO[2], 2, texture[2], shaders);   // ground
        drawObject(VAO[3], 12, texture[0], shaders);  // house2
        drawObject(VAO[4], 6, texture[1], shaders);   // roof2
        
        // Associate the unitary matrix to the model matrix (restart the position of the model)

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(radius*cos(timeValue), radius*sin(timeValue), 0.));
        model = glm::scale(model, glm::vec3(0.4f));

        model2 = glm::scale(model2, glm::vec3(0.02f));
        model2 = glm::translate(model2, glm::vec3(-600.0f,-200.0f,600.0f));
        glUniformMatrix4fv(modelLoc2, 1, GL_FALSE, glm::value_ptr(model2));
        object2.Draw(shaders);

        model3 = glm::scale(model3, glm::vec3(5.0f));
        model3 = glm::translate(model3, glm::vec3(2.0f,-2.0f,1.5f));

        //model3 = glm::translate(model3, glm::vec3(2.0f,-2.0f,1.5f));
        //model2 = glm::rotate(model2, 80.0f, glm::vec3(0.0f, 0.0f, 1.0f));
        glUniformMatrix4fv(modelLoc3, 1, GL_FALSE, glm::value_ptr(model3));
        object3.Draw(shaders);

        // Update the global variable of the shader
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

        
        // Redraw the object
        object.Draw(shaders);


        // Exchange rendering buffers to update the window with new content
        glfwSwapBuffers(window);
    }
    
    // Delete the objects and buffer that we created earlier
    glDeleteVertexArrays(5, VAO);
    glDeleteBuffers(5, VBO);
    glDeleteBuffers(5, EBO);
    
    // End of the program, we clean up the context created by the GLFW
    glfwTerminate();
    return 0;
}

void fillBuffer(GLuint &VAO, GLuint &VBO, GLuint &EBO, GLfloat vertices[],
    GLshort indices[], GLint vsize, GLint isize) {
    // Bind the VAO
    glBindVertexArray(VAO);

    // Bind and fill the VBO
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vsize, vertices, GL_STATIC_DRAW);
    // Bind and fill the EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, isize, indices, GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), 
        (GLvoid*)0);
    glEnableVertexAttribArray(0);
    // UV attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), 
        (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);
    // normal attribute
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
        (GLvoid*)(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);
    
    // VBO is detached from the current buffer in the OpenGL context
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // VAO is detached from the current object in the OpenGL context
    glBindVertexArray(0);
    // EBO is detached (the last one!)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); 
}

void drawObject(GLuint &VAO, GLint nVertex, GLuint &texture, Shader &shaders) {
    // Bind the VAO of house as a current object in the context of OpenGL
    glBindVertexArray(VAO);
    // Activate texture
    glActiveTexture(GL_TEXTURE0);
    // Bind the texture
    glBindTexture(GL_TEXTURE_2D, texture);
    // Associate the texture with the shader
    glUniform1i(glGetUniformLocation(shaders.Program, "modelTexture"), 0);
    // Draw the current object
    glDrawElements(GL_TRIANGLES, 3*nVertex, GL_UNSIGNED_SHORT, 0);
    // VBA is detached from the current object in the OpenGL context
    glBindVertexArray(0);
}

void loadTexture(GLuint &texture, const char* path) {
    // Bind the texture created in the global context of OpenGL
    glBindTexture(GL_TEXTURE_2D, texture); 
    // Method of wrapping
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); 
    // Filtering method
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    // Loading the image file using the SOIL lib
    int twidth, theight;
    unsigned char* data = SOIL_load_image(path, &twidth, &theight, 0, SOIL_LOAD_RGB);
    // Associate the image data with texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, twidth, theight, 
        0, GL_RGB, GL_UNSIGNED_BYTE, data);
    // Generate the mipmap
    glGenerateMipmap(GL_TEXTURE_2D);
    // Free the memory
    SOIL_free_image_data(data);
    // Unbind the texture
    glBindTexture(GL_TEXTURE_2D, 0);
}