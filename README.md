# Laura-Arthur-OpenGL

## Installation guide:


1. Clone the repository
2. launch the executable (./exercice in build)


if the executable does not work:

1. Change the folder path at lines: 87, 197, 198, 199 
2. In the folder build:
    cmake ..
    make
    ./exercice

## Camera keys

To switch to the mode where the camera is in the air, press "b"
To switch back to the "walking" mode, press "c"
