// Specify that we use version 3.3 of OpenGL
#version 330 core

// Recover the variable of the vertex shader
in vec2 texCoord;
in vec3 Normal; // normal to the vertex
in vec3 FragPos; // position of the vertex
// Value that is used to give color to the vertex
out vec4 color;

// Texture 
uniform sampler2D modelTexture;
uniform vec3 lightPos; // position of the light source
uniform vec3 lightColor; // color of the light sourc

void main()
{
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;

    color = vec4(diffuse, 1.0f) * texture(modelTexture, texCoord);
}